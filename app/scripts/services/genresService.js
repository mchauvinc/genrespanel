'use strict';

nooApp.factory('genresService', function($resource) {
    return $resource("http://epg.suterastudio.com/api/genres", {
        format: "json", callback: "JSON_CALLBACK"
    }, {
        get: {
            method: "JSONP"
        },
        query: {
            method: "JSONP",
            isArray: true
        },
        save: {
            method: "JSONP",
            isArray: true
        }
    });
});
