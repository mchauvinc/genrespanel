'use strict';

nooApp.controller('GenresCtrl', function ($scope, genresService) {
    $scope.genres = genresService.query();
    $scope.deleteSubgenre = function () {
        this.genre.subgenres = this.genre.subgenres.filter(angular.bind(this,function (x) {
            return x != this.subgenre;
        }));
    }

    $scope.addSubgenre = function () {
        // Check key unicity
        var subgenres = this.genre.subgenres,
            newKey = this.genre.newKey,
            uniq = !isNaN(parseInt(newKey)) && !subgenres.some(angular.bind(this,function (x) {
                return x.subgenre_key == newKey;
            }));
        if (uniq) {
            this.genre.subgenres.push({ title: "", subgenre_key: newKey });
        }
        // Empty the input
        this.genre.newKey = "";

        this.genre.invalidKey = !uniq;
    }

    $scope.doSave = function () {
        genresService.save({ data: angular.toJson($scope.genres) });
    }
});
