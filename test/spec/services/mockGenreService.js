'use strict';

describe('Service: mockGenreService', function () {

  // load the service's module
  beforeEach(module('nooApp'));

  // instantiate service
  var mockGenreService;
  beforeEach(inject(function(_mockGenreService_) {
    mockGenreService = _mockGenreService_;
  }));

  it('should do something', function () {
    expect(!!mockGenreService).toBe(true);
  });

});
