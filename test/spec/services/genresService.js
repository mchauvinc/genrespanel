'use strict';

describe('Service: genresService', function () {

  // load the service's module
  beforeEach(module('nooApp'));

  // instantiate service
  var genresService;
  beforeEach(inject(function(_genresService_) {
    genresService = _genresService_;
  }));

  it('should do something', function () {
    expect(!!genresService).toBe(true);
  });

});
