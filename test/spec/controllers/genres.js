'use strict';

describe('Controller: GenresCtrl', function() {

  // load the controller's module
  beforeEach(module('nooApp', 'mockGenreService', 'ngResource'));

  var GenresCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller, $resource, genresService) {
      scope = {
          genres: genresService.query()
      };
    GenresCtrl = $controller('GenresCtrl', {
        $scope: scope,
        $resource: $resource
    });
  }));

  describe('editing subgenres feature', function () {
      it('adds an element to a subgenres list', function () {
          expect(scope.genres.length).toBe(2);
      });
      it('removes element from a subgenres list', function () {
          expect(scope.awesomeThings.length).toBe(3);
      });
  });
});
